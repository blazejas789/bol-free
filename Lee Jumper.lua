
--Lee Sin Jumper by BotHappy
--Changelog
--	1.0 - First release
--	1.1 - Added trinket support, menu and a wardhelper
--  1.2 - Improved WardJumper

if myHero.charName ~= "LeeSin" then return end

function OnLoad()
	Variables()
	Version = "1.2"
	KEY = string.byte("G")
	Menu = scriptConfig("Lee Jumper", "LeeJump"..Version)
	Menu:addParam("jump", "WardJump", SCRIPT_PARAM_ONKEYDOWN, false, KEY)
	Menu:addParam("Circle", "Draw Ward Range", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("WardHelper", "Draw Ward Helper", SCRIPT_PARAM_ONOFF, true)
	Menu:addParam("MaxRange", "Jump on Ward at max range", SCRIPT_PARAM_ONOFF, true)
	Menu:permaShow("jump")
	PrintChat("<font color='#FFFFFF'> >> Lee Sin Jumper "..Version.." loaded! << </font>")
end

function OnTick()
	Checks()
	if Menu.jump then WardJump() end
	Reset()
end

function OnDraw()
	if Menu.Circle then
		DrawCircle(myHero.x, myHero.y, myHero.z, WardRange, 0xFF00FF)
	end
	if Menu.WardHelper and Menu.jump then
		DrawCircle(myHero.x, myHero.y, myHero.z, WardRange, ARGB(255,255,50,51))
		HelperPos = FindNearestNonWall(mousePos.x, mousePos.y, mousePos.z, WardRange, 20)
		if HelperPos then
			if GetDistance(mousePos) < WardRange then
				DrawCircle3D(HelperPos.x, HelperPos.y, HelperPos.z, 50, 2, ARGB(255, 255, 255, 255), 20)
			else
				DrawCircle3D(HelperPos.x, HelperPos.y, HelperPos.z, 50, 2, ARGB(255, 255, 0, 0), 20)
			end
		end
	end
end

function Variables()
	WardTable = {}
	WardReady = false
	--Trinket ID for future updates
	----3340,3350,3361,3362 - Warding Totem, Greater Totem, Greater Stealth Totem, Greater Vision Totem
	
	SWard, VWard, SStone, RSStone, Wriggles = 2044, 2043, 2049, 2045, 3154
	SWardSlot, VWardSlot, SStoneSlot, RSStoneSlot, WrigglesSlot = nil, nil, nil, nil, nil
	RSStoneReady, SStoneReady, SWardReady, VWardReady, WrigglesReady, TrinketReady = false, false, false, false, false, false
	WardRange = 600
	wRange = 700
	
end

function Checks()
	W1Ready = ((myHero:CanUseSpell(_W) == READY) and myHero:GetSpellData(_W).name == "BlindMonkWOne")
	
	--Ward Check
	SWardSlot = GetInventorySlotItem(SWard)
	VWardSlot = GetInventorySlotItem(VWard)
	SStoneSlot = GetInventorySlotItem(SStone) 
	RSStoneSlot = GetInventorySlotItem(RSStone)
	WrigglesSlot = GetInventorySlotItem(Wriggles)
	--Ward Checks
	RSStoneReady = (RSStoneSlot ~= nil and CanUseSpell(RSStoneSlot) == READY)
	SStoneReady = (SStoneSlot ~= nil and CanUseSpell(SStoneSlot) == READY)
	SWardReady = (SWardSlot ~= nil and CanUseSpell(SWardSlot) == READY)
	VWardReady = (VWardSlot ~= nil and CanUseSpell(VWardSlot) == READY)
	WrigglesReady = (WrigglesSlot ~= nil and CanUseSpell(WrigglesSlot) == READY)
	TrinketReady = myHero:CanUseSpell(ITEM_7) == READY and myHero:getItem(ITEM_7).id == 3340 or myHero:CanUseSpell(ITEM_7) == READY and myHero:getItem(ITEM_7).id == 3350 or myHero:CanUseSpell(ITEM_7) == READY and myHero:getItem(ITEM_7).id == 3361 or myHero:CanUseSpell(ITEM_7) == READY and myHero:getItem(ITEM_7).id == 3362
	--Got a ward to place to jump
	GotWard = WrigglesReady or RSStoneReady or SStoneReady or SWardReady or VWardReady or TrinketReady
end

function Reset()
	if not Menu.jump then
		for k,v in pairs(WardTable) do WardTable[k]=nil end
		WardReady = false
	end
end

function WardJump()
	MoveToCursor()
	local WardMyPos = Vector(myHero.x, myHero.y, myHero.z)
	local WardMousePos = Vector(mousePos.x, mousePos.y, mousePos.z)
	if Menu.MaxRange then
	 	if GetDistance(WardMousePos) < WardRange then
	 		Coordenates = mousePos
	 	else
	 		Coordenates = WardMyPos + (WardMousePos - WardMyPos):normalized()*(WardRange-25)
	 	end
	else
		Coordenates = mousePos
	end
	if W1Ready and GetDistance(Coordenates) <= WardRange and GotWard and not WardReady then
		if RSStoneReady then
			CastSpell(RSStoneSlot, Coordenates.x, Coordenates.z)
			WardReady = true
		elseif SStoneReady and not WardReady then
			CastSpell(SStoneSlot, Coordenates.x, Coordenates.z)
			WardReady = true
		elseif WrigglesReady and not WardReady then
			CastSpell(WrigglesSlot, Coordenates.x, Coordenates.z)
			WardReady = true
		elseif TrinketReady and not WardReady then
			CastSpell(ITEM_7, Coordenates.x, Coordenates.z)
			WardReady = true
		elseif SWardReady and not WardReady then
			CastSpell(SWardSlot, Coordenates.x, Coordenates.z)
			WardReady = true
		elseif VWardReady and not WardReady then
			CastSpell(VWardSlot, Coordenates.x, Coordenates.z)
			WardReady = true
		end
	end
	if W1Ready and WardReady then
		for _, Ward in ipairs(WardTable) do
			if GetDistance(Ward) < wRange then
				CastSpell(_W, Ward)
			end
		end
	end
end

function MoveToCursor()
	if GetDistance(mousePos) then
		local moveToPos = myHero + (Vector(mousePos) - myHero):normalized()*300
		myHero:MoveTo(moveToPos.x, moveToPos.z)
	end	
end

function FindNearestNonWall(x0, y0, z0, maxRadius, precision)
    local vec, radius = D3DXVECTOR3(x0, y0, z0), 1
    if not IsWall(vec) then return vec end
    x0, z0, maxRadius, precision = math.round(x0 / precision) * precision, math.round(z0 / precision) * precision, maxRadius and math.floor(maxRadius / precision) or math.huge, precision or 50
    local function checkP(x, y) 
        vec.x, vec.z = x0 + x * precision, z0 + y * precision 
        return not IsWall(vec) 
    end
    while radius <= maxRadius do
        if checkP(0, radius) or checkP(radius, 0) or checkP(0, -radius) or checkP(-radius, 0) then 
            return vec 
        end
        local f, x, y = 1 - radius, 0, radius
        while x < y - 1 do
            x = x + 1
            if f < 0 then 
                f = f + 1 + 2 * x
            else 
                y, f = y - 1, f + 1 + 2 * (x - y)
            end
            if checkP(x, y) or checkP(-x, y) or checkP(x, -y) or checkP(-x, -y) or 
               checkP(y, x) or checkP(-y, x) or checkP(y, -x) or checkP(-y, -x) then 
                return vec 
            end
        end
        radius = radius + 1
    end
end


function OnCreateObj(Object)
	if Object and Object.valid and (string.find(Object.name, "Ward") ~= nil or string.find(Object.name, "Wriggle") ~= nil) then 
		table.insert(WardTable, Object) 
	end
end